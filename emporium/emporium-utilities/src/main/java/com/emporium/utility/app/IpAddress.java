package com.emporium.utility.app;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class IpAddress {

	public static InetAddress findIP() {
		InetAddress ip = null;
		String hostname;
		try {
			ip = InetAddress.getLocalHost();
			hostname = ip.getHostName();
		} catch (UnknownHostException e) {

			e.printStackTrace();
		}
		return ip;
	}

	public static String findHost() {
		InetAddress ip;
		String hostname = null;
		try {
			ip = InetAddress.getLocalHost();
			hostname = ip.getHostName();
		} catch (UnknownHostException e) {

			e.printStackTrace();
		}
		return hostname;
	}

}
